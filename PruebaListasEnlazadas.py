'''
Created on 30 oct. 2020

@author: Cesar
'''

class Nodo:
    def __init__(self,dato=None):
        self.dato=dato
        self.nodoSiguiente=None
    
    def getdato(self):
        return self.dato
    
    def getnodoSiuiente(self):
        return self.nodoSiguiente
    
    def setDato(self,dato):
        self.dato=dato
    
    def setNodoSiguiente(self,siguiente):
        self.nodoSiguiente=siguiente
   
    def __str__(self):
        return f"Nodo dato= {self.dato} Nodo siguiente {self.nodoSiguiente}"
    
    
class ListaEnlazada():
    
    def correcion(self):
        e=1
        while e==1:
            try:
                r = int(input())
            except:
                print("Ups, solo numero enteros mayores a 0")
                e=1
            else:
                if r>0:
                    e=0
                else:
                    print("Ups, solo numeros entero mayores a 0")
                    e=1
        return r

    
    
    def __init__(self):
        self.nodoInicio = Nodo()
        self.nodoFinal= Nodo()
        self.nodoInicio.nodoSiguiente= self.nodoFinal
        self.ele=0
        
    def verificarVacia(self):
        if(self.ele==0):
            return True
        else:
            return False
        
    
    def enlace(self,a,r,b):
        a.nodoSiguiente=r
        r.nodoSiguiente=b
        self.ele=self.ele+1
        
    def insertarInicio(self,dato):
        self.enlace(self.nodoInicio,Nodo(dato),self.nodoInicio.nodoSiguiente)
        
    def insertarFinal(self,dato):
        aux=self.nodoInicio
        while aux.nodoSiguiente != self.nodoFinal:
            aux=aux.nodoSiguiente
        
        self.enlace(aux,Nodo(dato),aux.nodoSiguiente)

    def recorrerLista(self):
        if self.verificarVacia()==False:
            t=self.nodoInicio.nodoSiguiente
            print("===== Los elementos de la lista son =====")
            while t!=self.nodoFinal:
                print(f"[{t.dato}]--> ", end="")
                t=t.nodoSiguiente
            
            print()
        else:
            print("Lista Vacia")
    
    def eliminarInicio(self):
        if(self.verificarVacia()==False):
            self.nodoInicio.nodoSiguiente=self.nodoInicio.nodoSiguiente.nodoSiguiente
            self.ele=self.ele-1
        else:
            print("La lista esta vacia!")
            print("No se pude eliminar dato")
            
    def eliminarFinal(self):
        if(self.verificarVacia()==False):
            elemento=self.nodoInicio
            temporal=None
            while elemento.nodoSiguiente!=self.nodoFinal:
                temporal=elemento
                elemento=elemento.nodoSiguiente
            temporal.nodoSiguiente=self.nodoFinal
            self.ele=self.ele-1
        else:
            print("La lista esta vacia!")
            print("No se puede eliminar dato")
    
    
   
    def mostrarCantidadElementos(self):
        print(f"La cantidad de elementos es de: {self.ele}")
    
   
    
    
    
    
        
    
    
#------------------------------------------------------

op=0
lista = ListaEnlazada()
while(op!=6):
    print("=============== MENU ==============")
    print("Digite 1 para Verificar si la lista esta vacia")
    print("Digite 2 para Insertar un dati")
    print("Digite 3 para Eliminar un dato")
    print("Digite 4 para Mostrar la lista")
    print("Digite 5 para Mostrar la Cantidad de datos en la lista")
    print("Digite 6 para *SALIR*")
    op=lista.correcion()
    if(op==1):
        if(lista.verificarVacia()==True):
            print("La lista esta vacia")
        else:
            print("La lista *NO* esta vacia")
    elif(op==2):
        op2=0;
        
        while(not(op2==1 or op2==2) ):
            print("Digite 1 para  Insertar dato al Inicio de la lista")
            print("Dihite 2 para Insertar dato al Final de la lista")
            
            op2=lista.correcion()
            
            if(op2==1):
                dato=int(input("Ingrese el dato a inserta: "))
                lista.insertarInicio(dato)
            elif(op2==2):
                dato=int(input("Ingrese el dato a insertar: "))
                lista.insertarFinal(dato)
            else:
                print("Ups... Esta opcion es invalida")
    elif(op==3):
        op3=0
        print("==================================================")
        while(not(op3==1 or op3==2 ) ):
            
            print("Digite 1 para Eliminar dato al Inicio")
            print("Digite 2 para Eliminar dato al Final")
            op3=lista.correcion()
            if(op3==1):
                lista.eliminarInicio()
            elif(op3==2):
                lista.eliminarFinal()
           
            else:
                print("Ups esta opcion es invalida")
                
    elif(op==4):
        lista.recorrerLista()
        
    elif(op==5):
        lista.mostrarCantidadElementos()
        
    elif(op==6):
        print("================================")
        print("Usted ha salido del programa")
        print("Gracias por usar el programa")
    else:
        print("opcion incorrecta")

